countries = [
    { 'name' : 'Fribourg',
      'neighbors': ['Bern', 'Vaud', 'Neuchâtel']
    },
    { 'name' : 'Bern',
      'neighbors': ['Fribourg', 'Vaud', 'Neuchâtel']
    },
    { 'name' : 'Vaud',
      'neighbors': ['Fribourg', 'Bern', 'Neuchâtel']
    },
    { 'name' : 'Neuchâtel',
      'neighbors': ['Fribourg', 'Bern', 'Vaud']
    }
]

colors = ['red', 'green', 'blue', 'yellow']

import networkx as nx
G = nx.Graph()
for c in countries:
    G.add_node(c['name'])
for c in countries:
    for n in c['neighbors']:
        G.add_edge(c['name'], n)

dict = nx.brute_force_color(G)
if dict is None:
    print('failed')
else:
    for c in dict:
        print (c + ': ' + colors[dict[c]])
