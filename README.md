# 4-color theorem

## Description

This is a patchset for the [NextworkX](https://networkx.org/) python module to make it color maps. I [offered it](https://github.com/networkx/networkx/discussions/6672) to the NetworkX team, but they refused to commit it and didn's say why. That's why I'm publishing it here.

This is an implementation of the 4-color theorem, but it can also do less colors if possible (e.g. checkerboard), or more colors where 4 don't suffice (e.g. maps with disjunct countries). It can balance the colors to give a more or less equal distribution.

## Installation

Install Python3 and NetworkX if you haven't already, then copy src/networkx/algorithms/coloring/brute_force_coloring.py to algorithms/coloring/brute_force_coloring.py in your NetworkX dir and apply the __init__.py.diff patch.

## Licence

I make this available under the GPL v3 licence, but if it gets ever committed in NetworkX, their licence will apply in addition.
