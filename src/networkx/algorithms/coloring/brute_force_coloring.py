"""
Brute-force vertex coloring using compact-border strategy
"""

import networkx as nx

__all__ = ["brute_force_color"]


def brute_force_color(G, maxcolors=4, balance=False, startnode=None):
    """A brute-force coloring algorithm designed for 4-coloring real (e.g.
    geographic) maps using the strategy a human would normally use, i.e. to
    start on one side and keep the border between already colored and not yet
    colored nodes (countries) compact. This prevents deep recursion into
    impossible combinations and thus keeps performance acceptable for most maps.

    Parameters
    ----------
    G: NetworkX graph

    maxcolors: non-negative integer
         The maximum number of colors. If 0 or None, the smallest possible
         number of colors will be used. This takes slightly longer than when
         it is supplied.

    balance: boolean
         Whether to try to balance the number of nodes for each color. If
         False, there is a bias towards lower-indexed colors.

    startnode: NetworkX node
         Where to start calculation. If not supplied, a feasible start node is
         calculated. This node receives color 0.

    Returns
    -------
    If successful, a dictionary with keys representing nodes and values representing
    colors, starting with 0. Otherwise, None is returned.

    Raises
    ------
    NetworkXError
        If startnode is not a node in the graph.
    """

    if startnode is not None:
        if startnode not in G:
            raise nx.NetworkXError("startnode is not in the graph.")
    if not len(G):
        return {}
    n_in_order = []
    startind = 0
    while len(n_in_order) < len(G): # loop over unconnected parts of the graph
        if startnode is None:
            mincnt = None
            for n in G:
                if n not in n_in_order:
                    cnt = -2 * len(G.adj[n])
                    for n2 in G.adj[n]:
                        cnt += len(G.adj[n])
                    if mincnt is None or cnt < mincnt:
                        mincnt = cnt
                        startnode = n

        # determine sequence of nodes to calculate
        n_in_order.append(startnode)
        nodes_in_part = 1
        while len(n_in_order) < len(G):
            # determine stripe bordering already colored area
            stripe = set()
            for n in n_in_order[startind:]:
                for nbr in G.adj[n]:
                    if nbr not in n_in_order[startind:]:
                        stripe.add(nbr)
            if not len(stripe):
                break # path containes another unconnected part, handled in outer loop
            # calculate score for each node in stripe
            minscore = None
            optim_n = None
            for n in stripe:
                score = 0
                for n2 in n_in_order[startind:] + [n]:
                    on_border = 0
                    for nbr in G.adj[n2]:
                        if nbr in n_in_order[startind:]:
                            score -= 5
                        else:
                            on_border = 11
                            score += 1 # one border segment (edge crossing border); the more of them, the worse
                    score += on_border # the more nodes along border, the worse
                if minscore is None or score < minscore:
                    minscore = score
                    optim_n = n
            n_in_order.append(optim_n)
            nodes_in_part += 1
        startnode = None
        startind += nodes_in_part

    for numcolors in (maxcolors,) if maxcolors else (1,2,3,4):
        # greedy-color nodes in sequence and step back whenever getting stuck
        colors = dict((n, None) for n in G)
        color_transl = [[r for r in range(numcolors)]] * len(n_in_order)
        i = 0
        while i < len(n_in_order):
            n = n_in_order[i]
            transl_ind = -1 if colors[n] is None else color_transl[i].index(colors[n])
            if transl_ind + 1 == numcolors:
                colors[n] = None
                if i == numcolors:
                    break
                i -= 1
                continue
            colors[n] = color_transl[i][transl_ind + 1]
            color_available = True
            for nbr in G.adj[n]:
                if colors[nbr] == colors[n]:
                    color_available = False
                    break
            if color_available:
                i += 1
                if balance and i < len(n_in_order):
                    colcnt = [0] * numcolors
                    for j in range(i):
                        colcnt[colors[n_in_order[j]]] += 1
                    color_transl[i] = sorted([c for c in range(numcolors)], key=lambda c: colcnt[c])
        if colors[n_in_order[-1]] is not None:
            return colors
    return None
